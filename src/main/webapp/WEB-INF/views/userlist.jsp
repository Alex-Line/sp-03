<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
    Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
<div align="center">
    <h1>User List</h1>
    <h3><a href="/user/create">New User</a></h3>
    <table border="1" cellpadding="5">
        <tr>
            <td>ID</td>
            <td>Login</td>
            <td>Role</td>
            <td>Action</td>
        </tr>
            <c:forEach items="${users}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.login}</td>
                    <td>${user.role}</td>
                    <td>
                        <a href="/user/select?userId=${user.id}">select</a>
                        <a href="/user/view?userId=${user.id}">view</a>
                        <a href="/user/edit?userId=${user.id}">edit</a>
                        <a href="/user/delete?userId=${user.id}">delete</a>
                    </td>
                 </tr>
            </c:forEach>
    </table>
</div>
</body>
</html>