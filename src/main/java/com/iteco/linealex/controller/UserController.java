package com.iteco.linealex.controller;

import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.model.User;
import com.iteco.linealex.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.NoSuchAlgorithmException;

@Controller
public class UserController {

    @Autowired
    IUserService userService;

    @RequestMapping("/user/list")
    public ModelAndView listUsers() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("userlist");
        modelAndView.addObject("users", userService.getAllEntities());
        return modelAndView;
    }

    @RequestMapping("/user/view")
    public ModelAndView view(@NotNull final String userId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("userview");
        modelAndView.addObject("user", userService.getEntityById(userId));
        return modelAndView;
    }

    @RequestMapping("/user/edit")
    public ModelAndView edit(@NotNull final String userId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("useredit");
        @Nullable final User user = userService.getEntityById(userId);
        modelAndView.addObject("user", user);
        return modelAndView;
    }

    @RequestMapping("/user/delete")
    public String delete(@ModelAttribute("userId") String userId) {
        userService.removeEntity(userId);
        return "redirect:/user/list";
    }

    @RequestMapping(value = "/user/create", method = RequestMethod.GET)
    public ModelAndView create(
            @ModelAttribute("login") String login,
            @ModelAttribute("password") String password
    ) throws NoSuchAlgorithmException {
        @NotNull final ModelAndView modelAndView = new ModelAndView("usercreate");
        @NotNull final User createdUser = new User();
        createdUser.setLogin(login);
        createdUser.setHashPassword(TransformatorToHashMD5.getHash(password));
        modelAndView.addObject("user", createdUser);
        return modelAndView;
    }

    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("user") User user) throws NoSuchAlgorithmException {
        user.setHashPassword(TransformatorToHashMD5.getHash(user.getHashPassword()));
        @NotNull final User createdUser = new User();
        createdUser.setLogin(user.getLogin());
        createdUser.setHashPassword(TransformatorToHashMD5.getHash(user.getHashPassword()));
        userService.persist(user);
        return "redirect:/user/list";
    }

    @RequestMapping("/user/select")
    public ModelAndView select(@ModelAttribute("userId") String userId) {
        @NotNull final ModelAndView modelAndView =
                new ModelAndView("project_vs_task", "userId", userId);
        return modelAndView;
    }

}