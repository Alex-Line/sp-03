package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto {

    @Autowired
    static IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish;

    @Nullable
    String userId;

    Status status;

    @Nullable
    public static Project toProject(@NotNull final ProjectDto projectDto) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        @Nullable final User user = userService.getEntityById(projectDto.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        return project;
    }

}