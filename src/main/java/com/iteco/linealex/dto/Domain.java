package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.Task;
import com.iteco.linealex.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"users", "projects", "tasks"}, name = "Domain")
public class Domain implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @XmlElement(name = "user")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "users")
    private List<User> users;

    @Nullable
    @XmlElement(name = "project")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "tasks")
    private List<Task> tasks;

    public void load(
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) throws Exception {
        users = new LinkedList<>(userService.getAllEntities());
        projects = new LinkedList<>(projectService.getAllEntities());
        tasks = new LinkedList<>(taskService.getAllEntities());
    }

}